MODULE cardan
  implicit none

contains
    !================================================================
    !     methode de resolution exacte des  equations du troisieme degre “par radicaux”
    !     dite methode Cardan
    !     INPUT
    !     -----
    !     coeff    : tableau contenant les coefficients du polynome,
    !               dans l'ordre, a, b, c, d
    !     n: dimension du tableau
    !
    !     OUTPUT
    !     ------
    !     racines : racines du polynome (reelles uniquement)
    !     m: taille du tableau racines
    !     status  : nombre de racines ou -1 si erreur
    !
    !
    !     [1] https://www.math.u-psud.fr/~perrin/CAPES/algebre/Cardan10.pdf
    !
    !     [5] https://fr.wikipedia.org/wiki/%C3%89quation_cubique#Discriminant
    !
    !     Gérard HENRY, Mai 2020.
    !================================================================

  subroutine calcule_racines(coeff, n, racines, m, status)
    implicit none

    integer, intent(in) :: n, m
    integer, intent(out) :: status
    double precision, dimension(n), intent(in) :: coeff
    double precision, dimension(m), intent(out) :: racines
    double precision :: p, q, DELTA, t, delta1, delta2
    double precision :: a, b, c, d
    double precision :: temp1, temp2
    complex :: u, u3, v, v3
    integer :: i
    Complex :: j
    Complex :: j2

    j = dcmplx(- 0.5, sqrt(3.) / 2.)
    j2 = dcmplx(- 0.5, -sqrt(3.) / 2.)

#ifdef DEBUG
    write(*,*) __FILE__, ":", __LINE__, "coeff: ", coeff
#endif
    status = 0  ! (0 racines)

    i = 1
    a = coeff(i)
    i = i + 1
    b = coeff(i)
    i = i + 1
    c = coeff(i)
    i = i + 1
    d = coeff(i)

    ! calcul du discriminant de l'eq cubique https://fr.wikipedia.org/wiki/%C3%89quation_cubique#Discriminant
    delta1 = (18. * a * b * c * d) - (4 * (b**3) * d) + (b**2 * c**2) - (4 * a * c**3) - (27 * a**2 * d**2)
#ifdef DEBUG
    write(*, *) "determinant de l'eq. cubique initiale: ", delta1
    if (delta1 > 0) then
       write(*, *) "3 solutions réelles distinctes: "
    elseif (delta1 == 0.) then
       write(*, *) "determinant nul une solution muliple et réelle"
    else
       write(*, *) "determinant negatif, 1 solution réelle et deux complexes conjuguées"
    endif
#endif
    ! on calcule p et q, réduction pour faire disparaitre le terme en x**2: X**3 + p X + q
    p = (c / a) - (((b / a)**2) / 3.)
    q = ((b / (27. * a)) * (((2. * b**2) / a**2) - (9. * c / a))) + (d / a)
#ifdef DEBUG
    write(*, *) "p: ", p, "q: ", q
    write(*, *) "pour X**3 + p X + q = 0 et X = x + b/3a"
#endif
    ! calcul du determinant de l'équation cubique X**3 + p X + q = 0
    DELTA = -((27. * q**2) + (4. * p**3))
#ifdef DEBUG
    write(*, *) "DELTA: ", DELTA
    ! calcul du petit determinant de l'eq 2nd degre: X2 + qX -p³/27
    write(*, *) "petit delta: ", q**2 + ((4.*p**3)/27.), "DELTA / -27: ", -DELTA/27.

#endif

    if (DELTA > 0.) then
#ifdef DEBUG
       write(*, *) "3 solutions réelles distinctes: "
#endif
       u3 = (dcmplx(-q, sqrt(DELTA / 27.))) / 2.
#ifdef DEBUG
       write(*, *) "u3: ", u3
#endif
       u = u3**(1. / 3.)
#ifdef DEBUG
       write(*, *) "u: ", u
#endif
       racines(1) = (u + conjg(u)) - (b / (3. * a))
       racines(2) = ((j * u) + conjg(j * u)) - (b / (3. * a))
       racines(3) = ((j2 * u) + conjg(j2 * u)) - (b / (3. * a))
       status = 3
#ifdef DEBUG
       ! verification
       write(*, *) "u + conjg(u) ", u + conjg(u)
#endif
    elseif (DELTA < 0.) then
#ifdef DEBUG
       write(*, *) __FILE__, ":", __LINE__, "determinant negatif, &
		&1 solution reelle et deux complexes"
#endif
       u3 = (-q + sqrt(-DELTA / 27.)) / 2.
       v3 = (-q - sqrt(-DELTA / 27.)) / 2.
#ifdef DEBUG
       write(*, *) "u3: ", u3, "v3: ", v3
       write(*, *) "u3+v3: ", u3+v3
       write(*, *) "u3*v3: ", u3*v3
#endif
       u = cbrt(real(u3))
       v = cbrt(real(v3))
#ifdef DEBUG
       write(*, *) "u: ", u, "v: ", v
       write(*, *) "u*v: ", u*v, " = -p/3: ", -p/3
       write(*, *) "u+v: ", u+v
#endif
       racines(1) = u + v - (b / (3. * a))
       status = 1 ! on ignore les racines complexes pour l'instant
    else ! (DELTA = 0)
#ifdef DEBUG
       write(*, *) __FILE__, ":", __LINE__, "determinant nul une &
	&solution muliple et reelle"
#endif
       if ((p == 0.) .and. (q == 0.)) then
          racines(1) = 0
          status = 3
       else
          racines(1) = (3. * q / p) - (b / (3. * a))
          racines(2) = -(3. * q / p) - (b / (3. * a))
          status = 2
       end if
    end if

  end subroutine calcule_racines

    !================================================================
    ! calcul de la recine cubique (cube root)
    !================================================================

  FUNCTION cbrt(x)
    IMPLICIT NONE

    real :: x, cbrt

    cbrt = sign(1., x) * (abs(x)**(1. / 3.))

    RETURN
  END FUNCTION cbrt

END MODULE cardan
