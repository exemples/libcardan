PROGRAM test_arg
    USE cardan
    use constants
    implicit none

    INTEGER :: i
    CHARACTER(len = 32) :: arg
    integer, parameter :: n = 4, m = 3
    double precision, dimension(n) :: coeff
    double precision :: a, b, c, d, sol
    equivalence (a, coeff(1)), (b, coeff(2)), (c, coeff(3)), (d, coeff(4))
    double precision, dimension(m) :: rac
    integer :: status
    real :: resul

    i = 1
    coeff = 0.
    DO
        CALL get_command_argument(i, arg)
        IF (LEN_TRIM(arg) == 0) EXIT

        !WRITE (*, *) TRIM(arg)
        if (i > 0) then
            read (arg, *) coeff(i)
#ifdef DEBUG
             write(*,*) __FILE__, ":", __LINE__, "coeff[", i, "]=", coeff(i)
#endif
        end if
        i = i + 1
    END DO

    if (i > 1) then
        ! on lit les coffs sur l'entrée standard
        write(*, "(A/)", advance = "yes") " "
        call calcule_racines(coeff, n, rac, m, status)
        if (status > -1) then
            do i = 1, status
                write(*, *) "racine: ", rac(i)
            end do
            call verif(coeff, n, rac, m, status)
        else
            write(*, *) "ERREUR: ", rac(1)
        end if
        stop
    else
        ! https://fr.wikiversity.org/wiki/%C3%89quation_du_troisi%C3%A8me_degr%C3%A9/Exercices/R%C3%A9solution_par_la_m%C3%A9thode_de_Cardan#Exercice_4-1
        write(*, *) ""
        write(*, *) "*************"
        write(*, *) "test "
        write(*, *) "*************"
        coeff(1) = 6.
        coeff(2) = -6.
        coeff(3) = 12.
        coeff(4) = 7.
        sol = ((2.5**(1. / 3.)) - (50**(1. / 3.)) + 1.) / 3.
        write(*, *) "Solution exacte: ", sol
        call poly(coeff, n, sol, status)
        call calcule_racines(coeff, n, rac, m, status)
        if (status > -1) then
            do i = 1, status
                write(*, *) "racine: ", rac(i)
            end do
            call verif(coeff, n, rac, m, status)
        else
            write(*, *) "ERREUR: ", rac(1)
        end if

    end if

    ! https://fr.wikiversity.org/wiki/%C3%89quation_du_troisi%C3%A8me_degr%C3%A9/Exercices/R%C3%A9solution_par_la_m%C3%A9thode_de_Cardan#Exercice_4-1
    write(*, *) "*************"
    write(*, *) "test "
    write(*, *) "*************"

    coeff(1) = 1.
    coeff(2) = -6.
    coeff(3) = 9.
    coeff(4) = -1.
    sol = (2.*cos(2.*PI/9.)) + 2.  !   (2.*cos(8.*PI/9.) + 2.)  (2.*cos(4.*PI/9.) + 2.)
    write(*, *) "Solution exacte: ", sol
    call poly(coeff, n, sol, status)
    call calcule_racines(coeff, n, rac, m, status)
    if (status > -1) then
        do i = 1, status
            write(*, *) "racine: ", rac(i)
            call verif(coeff, n, rac, m, status)
        end do
    else
        write(*, *) "ERREUR: ", rac(1)
    end if

    ! https://fr.wikiversity.org/wiki/%C3%89quation_du_troisi%C3%A8me_degr%C3%A9/Exercices/R%C3%A9solution_par_la_m%C3%A9thode_de_Cardan#Exercice_4-1
    write(*, *) "*************"
    write(*, *) "test "
    write(*, *) "*************"

    coeff(1) = 1.
    coeff(2) = -2.
    coeff(3) = 3.
    coeff(4) = -2.
    sol = 1
    write(*, *) "Solution exacte: ", sol
    call poly(coeff, n, sol, status)
    call calcule_racines(coeff, n, rac, m, status)
    if (status > -1) then
        do i = 1, status
            write(*, *) "racine: ", rac(i)
            call verif(coeff, n, rac, m, status)
        end do
    else
        write(*, *) "ERREUR: ", rac(1)
    end if

    ! https://fr.wikiversity.org/wiki/%C3%89quation_du_troisi%C3%A8me_degr%C3%A9/Exercices/R%C3%A9solution_par_la_m%C3%A9thode_de_Cardan#Exercice_4-1
    write(*, *) "*************"
    write(*, *) "test : pas de racines réelles"
    write(*, *) "*************"

    coeff(1) = 3.
    coeff(2) = 9.
    coeff(3) = -9.
    coeff(4) = -29.
    sol = 1
    call calcule_racines(coeff, n, rac, m, status)
    if (status > -1) then
        do i = 1, status
            write(*, *) "racine: ", rac(i)
            call verif(coeff, n, rac, m, status)
        end do
    else
        write(*, *) "ERREUR: ", rac(1)
    end if

END PROGRAM test_arg

subroutine verif(coeff, n, rac, m, status)
    integer, intent(in) :: n, m
    double precision, dimension(n), intent(in) :: coeff
    integer, intent(out) :: status
    double precision, dimension(m), intent(in) :: rac

    ! verification
    call poly(coeff, n, rac(1), status)

end subroutine verif

subroutine poly(coeff, n, x, status)
    integer, intent(in) :: n
    double precision, dimension(n), intent(in) :: coeff
    double precision, intent(in) :: x
    integer, intent(out) :: status
    real :: resul

    ! verification
    resul = coeff(1) * x**3 + coeff(2) * x**2 + coeff(3) * x + coeff(4)
    write(*, *) "verif de ", coeff(1), " x3 + ", coeff(2), " x2 + ", coeff(3), " x + ", coeff(4), " =  0"
    if (resul < 1.d-3) then
        write(*, *) "verif ok: ", resul
        status = 0
    else
        write(*, *) "verif ko: ", resul
        status = -1
    end if

end subroutine poly
! * * * * * * * 


