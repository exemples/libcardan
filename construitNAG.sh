#!/bin/bash

#set -x

# En cas d'erreur, penser à effacer le dossier build

# c'est une seule ligne!!!

mkdir -p build/debug; \
cd build/debug; \
if [ -e /usr/local/bin/nagfor ]; then \
 cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=Debug -DCMAKE_VERBOSE_MAKEFILE:BOOL=ON -DCMAKE_Fortran_FLAGS_DEBUG="-g -w -fpp -DDEBUG -C=all -colour " -DCMAKE_Fortran_COMPILER=nagfor . ../..; \
else \
 cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=Debug -DCMAKE_VERBOSE_MAKEFILE:BOOL=ON -DCMAKE_Fortran_FLAGS_DEBUG="-g -O0 -Warray-bounds -w -cpp -DDEBUG -Wextra -Wimplicit-interface -fmax-errors=1 -fcheck=all -Wall -fbacktrace -fcheck=bound -ffree-line-length-0 " . ../..; \
fi; \
make ; \

